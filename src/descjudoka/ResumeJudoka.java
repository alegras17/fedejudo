
package descjudoka;

public class ResumeJudoka {
     
    public String    nom; 
    public String    prenom;
    public String    sexe;
    public String    dateNaiss;    // pour l'affichage la date sera convertie en chaine de caractères
    public int       poids;
    public String    ville;
    public int       nbVictoires; 
    public String    ceinture;
    public String    categorie;
    public int       age; 

    //<editor-fold defaultstate="collapsed" desc="gets sets">
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String getPrenom() {
        return prenom;
    }
    
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    public String getSexe() {
        return sexe;
    }
    
    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getDateNaiss() {
        return dateNaiss;
    }

    public void setDateNaiss(String dateNaiss) {
        this.dateNaiss = dateNaiss;
    }
    
   
    
    public int getPoids() {
        return poids;
    }
    
    public void setPoids(int poids) {
        this.poids = poids;
    }
    
    public String getVille() {
        return ville;
    }
    
    public void setVille(String ville) {
        this.ville = ville;
    }
    
    public int getNbVictoires() {
        return nbVictoires;
    }
    
    public void setNbVictoires(int nbVictoires) {
        this.nbVictoires = nbVictoires;
    }
    
    public String getCeinture() {
        return ceinture;
    }
    
    public void setCeinture(String ceinture) {
        this.ceinture = ceinture;
    }
    
    public String getCategorie() {
        return categorie;
    }
    
    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }
    
    public int getAge() {
        return age;
    }
    
    public void setAge(int age) {
        this.age = age;
    }
    
    
    //</editor-fold>
      
}
