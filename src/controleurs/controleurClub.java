package controleurs;

import descjudoka.ResumeJudoka;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;



public class controleurClub {
    
    //<editor-fold defaultstate="collapsed" desc="connexion">
    Connection  cx;
    
    
    
    public void init() throws Exception{
        
        cx= connexionBdd();
        if (cx!=null){ System.out.println("CONNEXION OK");}
        
    }
    
    
    Connection connexionBdd() throws Exception
            
    {
        
        Connection connexion=null;
        Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        connexion =  DriverManager.getConnection("jdbc:derby://localhost:1527/BaseFederationJudo","uJudo","mdp") ;
        return connexion;
    }
    //</editor-fold>
    
        String codeclub;
        String nomclub;
        String adresseclub;
        int effectif;
        int effectiffemmes;
        int effectifhommes;
        float moyenneage;
        float poidsmoyenfemmes;
        float poidsmoyenhommes;
                 
public static List<ResumeJudoka> listeDesJudokas= new LinkedList(); static {
           
}

public void rechercher() throws SQLException {
           String requete= "Select  adrclub, nomclub " +
                           "From   CLUB " +
                           "Where CODECLUB='"+getCodeclub()+"'";
        
            Statement   cmde   = cx.createStatement();
            ResultSet   clb   = cmde.executeQuery(requete);
            
            clb.next();
            setAdresseclub(clb.getString(1));
            setNomclub(clb.getString(2));
            
            String requete2= "Select  * " +
                             "From   JUDOKA INNER JOIN CLUB on JUDOKA.LECLUB_CODECLUB = CLUB.CODECLUB " +
                             "Where CLUB.CODECLUB='"+getCodeclub()+"'";
            
            Statement   cmde2   = cx.createStatement();
            ResultSet   jdk   = cmde2.executeQuery(requete2);
            
            while(jdk.next()){
                
                ResumeJudoka p1= new ResumeJudoka();
                p1.setNom(jdk.getString("nom")); 
                p1.setPrenom(jdk.getString("prenom"));
                p1.setSexe(jdk.getString("sexe"));
                p1.setDateNaiss(jdk.getString("datenaiss"));
                p1.setPoids(jdk.getInt("poids"));
                p1.setVille(jdk.getString("ville"));
                p1.setNbVictoires(jdk.getInt("nbvictoires")); 
                p1.setCeinture(jdk.getString("ceinture"));
                p1.setCategorie(utilitaires.UtilDojo.determineCategorie(jdk.getString("sexe"), jdk.getInt("poids")));
                p1.setAge(utilitaires.UtilDate.ageEnAnnees(jdk.getDate("datenaiss")));
                
                listeDesJudokas.add(p1);
                
            }
            
                System.out.println(listeDesJudokas);
}        
        
                 
    //<editor-fold defaultstate="collapsed" desc="code généré">
                 public static final String PROP_POIDSMOYENHOMMES = "poidsmoyenhommes";
                 
                 public float getPoidsmoyenhommes() {
                     return poidsmoyenhommes;
                 }
                 
                 public void setPoidsmoyenhommes(float poidsmoyenhommes) {
                     float oldPoidsmoyenhommes = this.poidsmoyenhommes;
                     this.poidsmoyenhommes = poidsmoyenhommes;
                     propertyChangeSupport.firePropertyChange(PROP_POIDSMOYENHOMMES, oldPoidsmoyenhommes, poidsmoyenhommes);
                 }
                 
                 public static final String PROP_POIDSMOYENFEMMES = "poidsmoyenfemmes";
                 
                 public float getPoidsmoyenfemmes() {
                     return poidsmoyenfemmes;
                 }
                 
                 public void setPoidsmoyenfemmes(float poidsmoyenfemmes) {
                     float oldPoidsmoyenfemmes = this.poidsmoyenfemmes;
                     this.poidsmoyenfemmes = poidsmoyenfemmes;
                     propertyChangeSupport.firePropertyChange(PROP_POIDSMOYENFEMMES, oldPoidsmoyenfemmes, poidsmoyenfemmes);
                 }
                 
                 public static final String PROP_MOYENNEAGE = "moyenneage";
                 
                 public float getMoyenneage() {
                     return moyenneage;
                 }
                 
                 public void setMoyenneage(float moyenneage) {
                     float oldMoyenneage = this.moyenneage;
                     this.moyenneage = moyenneage;
                     propertyChangeSupport.firePropertyChange(PROP_MOYENNEAGE, oldMoyenneage, moyenneage);
                 }
                 
                 public static final String PROP_EFFECTIFHOMMES = "effectifhommes";
                 
                 public int getEffectifhommes() {
                     return effectifhommes;
                 }
                 
                 public void setEffectifhommes(int effectifhommes) {
                     int oldEffectifhommes = this.effectifhommes;
                     this.effectifhommes = effectifhommes;
                     propertyChangeSupport.firePropertyChange(PROP_EFFECTIFHOMMES, oldEffectifhommes, effectifhommes);
                 }
                 
                 public static final String PROP_EFFECTIFFEMMES = "effectiffemmes";
                 
                 public int getEffectiffemmes() {
                     return effectiffemmes;
                 }
                 
                 public void setEffectiffemmes(int effectiffemmes) {
                     int oldEffectiffemmes = this.effectiffemmes;
                     this.effectiffemmes = effectiffemmes;
                     propertyChangeSupport.firePropertyChange(PROP_EFFECTIFFEMMES, oldEffectiffemmes, effectiffemmes);
                 }
                 
                 public static final String PROP_EFFECTIF = "effectif";
                 
                 public int getEffectif() {
                     return effectif;
                 }
                 
                 public void setEffectif(int effectif) {
                     int oldEffectif = this.effectif;
                     this.effectif = effectif;
                     propertyChangeSupport.firePropertyChange(PROP_EFFECTIF, oldEffectif, effectif);
                 }
                 
                 public static final String PROP_ADRESSECLUB = "adresseclub";
                 
                 public String getAdresseclub() {
                     return adresseclub;
                 }
                 
                 public void setAdresseclub(String adresseclub) {
                     String oldAdresseclub = this.adresseclub;
                     this.adresseclub = adresseclub;
                     propertyChangeSupport.firePropertyChange(PROP_ADRESSECLUB, oldAdresseclub, adresseclub);
                 }
                 
                 public static final String PROP_NOMCLUB = "nomclub";
                 
                 public String getNomclub() {
                     return nomclub;
                 }
                 
                 public void setNomclub(String nomclub) {
                     String oldNomclub = this.nomclub;
                     this.nomclub = nomclub;
                     propertyChangeSupport.firePropertyChange(PROP_NOMCLUB, oldNomclub, nomclub);
                 }
                 
                 public static final String PROP_CODECLUB = "codeclub";
                 
                 public String getCodeclub() {
                     return codeclub;
                 }
                 
                 public void setCodeclub(String codeclub) {
                     String oldCodeclub = this.codeclub;
                     this.codeclub = codeclub;
                     propertyChangeSupport.firePropertyChange(PROP_CODECLUB, oldCodeclub, codeclub);
                 }
                 private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
                 
                 public void addPropertyChangeListener(PropertyChangeListener listener) {
                     propertyChangeSupport.addPropertyChangeListener(listener);
                 }
                 
                 public void removePropertyChangeListener(PropertyChangeListener listener) {
                     propertyChangeSupport.removePropertyChangeListener(listener);
                 }
                 
                 
                 //</editor-fold>
}
