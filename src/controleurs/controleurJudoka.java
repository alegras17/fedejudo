package controleurs;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import utilitaires.UtilDate.*;

public class controleurJudoka {
    
        Connection  cx;
    
    

    public void init() throws Exception{
    
          cx= connexionBdd();
          if (cx!=null){ System.out.println("CONNEXION OK");}
    
    }
    
     
    Connection connexionBdd() throws Exception
    
    {
        
        Connection connexion=null;
        Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        connexion =  DriverManager.getConnection("jdbc:derby://localhost:1527/BaseFederationJudo","uJudo","mdp") ;
        return connexion;
    }  
    
    
    
        String nom;
        String prenom;
        String sexe;
        String club;
        String datenaiss;
        int age;
        String categoriepoids;
        String ceinture;
        int victoires;
        long id;
        float poids;
        float taille;
        float imc;

            
            
        public void recherche() throws SQLException {
            
           String requete= "Select  nom, prenom, sexe, leclub_codeclub, datenaiss, poids, taille, ceinture, nbvictoires " +
                           "From   Judoka " +
                           "Where ID="+id;
        
            Statement   cmde   = cx.createStatement();
            ResultSet   pers   = cmde.executeQuery(requete);
            
            pers.next();
            setNom(pers.getString(1));
            setPrenom(pers.getString(2));
            setSexe(pers.getString(3));
            setClub(pers.getString(4));
            setDatenaiss(pers.getString(5));
            setPoids(pers.getFloat(6));
            setTaille(pers.getFloat(7)/100);
            setCeinture(pers.getString(8));
            setVictoires(pers.getInt(9));
            setAge(utilitaires.UtilDate.ageEnAnnees(pers.getDate(5)));
            setCategoriepoids(utilitaires.UtilDojo.determineCategorie(pers.getString(3), pers.getInt(6)));
            float imc=poids/(taille*taille);
            setImc(imc);
        }
        
        
        
        //<editor-fold defaultstate="collapsed" desc="code genere">
        public static final String PROP_VICTOIRES = "victoires";
        
        public int getVictoires() {
            return victoires;
        }
        
        public void setVictoires(int victoires) {
            int oldVictoires = this.victoires;
            this.victoires = victoires;
            propertyChangeSupport.firePropertyChange(PROP_VICTOIRES, oldVictoires, victoires);
        }
        
        
        public static final String PROP_CEINTURE = "ceinture";
        
        public String getCeinture() {
            return ceinture;
        }
        
        public void setCeinture(String ceinture) {
            String oldCeinture = this.ceinture;
            this.ceinture = ceinture;
            propertyChangeSupport.firePropertyChange(PROP_CEINTURE, oldCeinture, ceinture);
        }
        
        
        public static final String PROP_CATEGORIEPOIDS = "categoriepoids";
        
        public String getCategoriepoids() {
            return categoriepoids;
        }
        
        public void setCategoriepoids(String categoriepoids) {
            String oldCategoriepoids = this.categoriepoids;
            this.categoriepoids = categoriepoids;
            propertyChangeSupport.firePropertyChange(PROP_CATEGORIEPOIDS, oldCategoriepoids, categoriepoids);
        }
        
        public static final String PROP_AGE = "age";
        
        public int getAge() {
            return age;
        }
        
        public void setAge(int age) {
            int oldAge = this.age;
            this.age = age;
            propertyChangeSupport.firePropertyChange(PROP_AGE, oldAge, age);
        }
        
        
        public static final String PROP_DATENAISS = "datenaiss";
        
        public String getDatenaiss() {
            return datenaiss;
        }
        
        public void setDatenaiss(String datenaiss) {
            String oldDatenaiss = this.datenaiss;
            this.datenaiss = datenaiss;
            propertyChangeSupport.firePropertyChange(PROP_DATENAISS, oldDatenaiss, datenaiss);
        }
        
        
        public static final String PROP_CLUB = "club";
        
        public String getClub() {
            return club;
        }
        
        public void setClub(String club) {
            String oldClub = this.club;
            this.club = club;
            propertyChangeSupport.firePropertyChange(PROP_CLUB, oldClub, club);
        }
        
        
        public static final String PROP_SEXE = "sexe";
        
        public String getSexe() {
            return sexe;
        }
        
        public void setSexe(String sexe) {
            String oldSexe = this.sexe;
            this.sexe = sexe;
            propertyChangeSupport.firePropertyChange(PROP_SEXE, oldSexe, sexe);
        }
        
        
        public static final String PROP_PRENOM = "prenom";
        
        public String getPrenom() {
            return prenom;
        }
        
        public void setPrenom(String prenom) {
            String oldPrenom = this.prenom;
            this.prenom = prenom;
            propertyChangeSupport.firePropertyChange(PROP_PRENOM, oldPrenom, prenom);
        }
        
        public static final String PROP_NOM = "nom";
        
        public String getNom() {
            return nom;
        }
        
        public void setNom(String nom) {
            String oldNom = this.nom;
            this.nom = nom;
            propertyChangeSupport.firePropertyChange(PROP_NOM, oldNom, nom);
        }
        private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
        
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.addPropertyChangeListener(listener);
        }
        
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.removePropertyChangeListener(listener);
        }
        
         public static final String PROP_ID = "id";

    public long getId() {
        return id;
    }

    public void setId(long id) {
        long oldId = this.id;
        this.id = id;
        propertyChangeSupport.firePropertyChange(PROP_ID, oldId, id);
    }
    
        public static final String PROP_TAILLE = "taille";

    public float getTaille() {
        return taille;
    }

    public void setTaille(float taille) {
        float oldTaille = this.taille;
        this.taille = taille;
        propertyChangeSupport.firePropertyChange(PROP_TAILLE, oldTaille, taille);
    }
    
    public static final String PROP_POIDS = "poids";

    public float getPoids() {
        return poids;
    }

    public void setPoids(float poids) {
        float oldPoids = this.poids;
        this.poids = poids;
        propertyChangeSupport.firePropertyChange(PROP_POIDS, oldPoids, poids);
    }
    
        public static final String PROP_IMC = "imc";

    public float getImc() {
        return imc;
    }

    public void setImc(float imc) {
        float oldImc = this.imc;
        this.imc = imc;
        propertyChangeSupport.firePropertyChange(PROP_IMC, oldImc, imc);
    }

        //</editor-fold>

   
}
